# LootTable

- LootTable is optimized for Getting random item from the collection. Adding is slow. Create your LootTables on startup.

This is a C# class that represents a loot table, which is a data structure used to randomly select items from a list with the probability of each item being chosen based on its weight.

The loot table is a generic class, which means it can be used with any type of item. It stores a list of tuples, each containing an item and an integer weight. It also has an array of prefix sums, which is used to efficiently select a random item from the list based on its weight. The prefix sums array is built by summing up the weights of the items in the list.

The loot table has a constructor that initializes the items list and prefix sums array, and also creates a new instance of the Random class. The Random class is used to generate random numbers.

The loot table has three methods:

- Add: This method adds an item to the loot table with a given weight. It also calls the BuildPrefixSums method to update the prefix sums array.

- GetRandom: This method selects a random item from the loot table based on its weight. It generates a random weight using the Random object, and then uses the Array.BinarySearch method to find the index of the item in the list that corresponds to the random weight. If the index is negative, it means that the value was not found in the array, so the ~index expression is used to get the index of the next highest value in the array. The item at this index is then returned.

- BuildPrefixSums: This method builds the prefix sums array by summing up the weights of the items in the list. It is called every time an item is added to the loot table to ensure that the prefix sums are up to date.

## Example

Sure, here is a simple example of how to use the LootTable class:

```C#
LootTable<string> lootTable = new LootTable<string>();

lootTable.Add("Gold Coin", 50);
lootTable.Add("Silver Coin", 25);
lootTable.Add("Bronze Coin", 10);

// Get a random item from the loot table
string randomItem = lootTable.GetRandom();

Console.WriteLine(randomItem);
```

In this example, we create a new instance of the LootTable class, which is a loot table of strings. We then add three items to the loot table: a gold coin with a weight of 50, a silver coin with a weight of 25, and a bronze coin with a weight of 10.

We then call the GetRandom method to select a random item from the loot table. The probability of each coin being chosen is based on its weight: the gold coin has a 50% chance of being chosen, the silver coin has a 25% chance, and the bronze coin has a 10% chance.

Finally, we print the random item to the console.