# CsvParser

- this parser uses reflection which makes it slow in runtime. Load your data on startup.

This is a generic CSV parser class in C#. It allows you to parse a CSV file and map its contents to instances of a specific type T. The class has the following features:

- It has a constructor that takes the file path of the CSV file, a delimiter character (which is a comma by default), and a decimal separator string (which is a period by default).
- It has a method SetCulture that allows you to specify the culture to use when converting strings to other data types.
- It has a method AddConversionFunction that allows you to specify a custom conversion function for a specific property of type T. This function will be used to convert the string value of a CSV cell to the appropriate data type for the property.
- It has a method Parse that reads the CSV file, maps its contents to a list of T instances, and returns the list.
- It has several private helper methods: ParseTitles, CreateList, CreateInstance, and MapValuesToProperties. ParseTitles parses the first line of the CSV file (which should contain the titles of the columns) and stores the indexes of the titles in a dictionary. CreateList creates an instance of a List<T>. CreateInstance creates an instance of T. MapValuesToProperties maps the values of a CSV line to the properties of an instance of T.

To use this class, you need to specify the type T and provide the file path of the CSV file you want to parse. You can also specify a custom delimiter and decimal separator if needed, and specify custom conversion functions for specific properties if necessary. Then, you can call the Parse method to parse the CSV file and get a list of T instances.

## Example

Here's an example of how you could use this class:

```c#
using System;
using System.Globalization;
using System.Collections.Generic;

public class Person
{
    public string Name { get; set; }
    public int Age { get; set; }
    public DateTime Birthday { get; set; }
    public decimal Salary { get; set; }
}

public static void Main()
{
    // Create a CSV parser for the "people.csv" file with a semicolon delimiter and a comma decimal separator.
    CsvParser<Person> parser = new CsvParser<Person>("people.csv", ';', ',');

    // Set the culture to "en-US" to use US-style date formatting.
    parser.SetCulture(new CultureInfo("en-US"));

    // Add a custom conversion function for the "Birthday" property to parse dates using the "dd/MM/yyyy" format.
    parser.AddConversionFunction("Birthday", x => DateTime.ParseExact(x, "dd/MM/yyyy", CultureInfo.InvariantCulture));

    // Parse the CSV file and get a list of Person instances.
    List<Person> people = parser.Parse();
}
```

In this example, we create a CSV parser for a file called "people.csv" that has a semicolon as the delimiter and a comma as the decimal separator. We set the culture to "en-US" to use US-style date formatting, and add a custom conversion function for the "Birthday" property to parse dates using the "dd/MM/yyyy" format. Then, we call the Parse method to parse the CSV file and get a list of Person instances.

Note that the CSV file should have a header row with the titles of the columns, and each row should contain the values for a single person in the same order as the titles. For example:

```
Name;Age;Birthday;Salary
Alice;25;01/01/1997;1000
Bob;30;15/07/1992;2000
```

This CSV file has four columns: "Name", "Age", "Birthday", and "Salary", and two rows with the values for Alice and Bob. When you call Parse, the class will create two Person instances and set their properties according to the values in the CSV file.