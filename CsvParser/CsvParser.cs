using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System;
using System.IO;
using System.Linq;

namespace CSV
{
    public class CsvParser<T>
    {
        private readonly string _filePath;
        private readonly Dictionary<string, int> _titles;
        private CultureInfo _cultureInfo;
        private readonly char _delimitter;

        private readonly Dictionary<string, Func<string, object>> _conversionFunctions;

        public CsvParser(string filePath, char delimitter = ',', string decimalSeparator = ".")
        {
            _filePath = filePath;
            _titles = new Dictionary<string, int>();
            _cultureInfo = new CultureInfo(CultureInfo.CurrentCulture.Name);
            _cultureInfo.NumberFormat.NumberDecimalSeparator = decimalSeparator;
            _delimitter = delimitter;
            _conversionFunctions = new Dictionary<string, Func<string, object>>();
        }

        public void SetCulture(CultureInfo culture)
        {
            _cultureInfo = culture;
        }

        public void AddConversionFunction(string propertyName, Func<string, object> conversionFunction)
        {
            _conversionFunctions[propertyName] = conversionFunction;
        }

        public List<T> Parse()
        {
            try
            {
                using (StreamReader reader = new StreamReader(_filePath))
                {
                    ParseTitles(reader.ReadLine());

                    List<T> result = CreateList();
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        T instance = CreateInstance();
                        MapValuesToProperties(instance, line);
                        result.Add(instance);
                    }

                    return result;
                }
            }
            catch (FileNotFoundException ex)
            {
                throw new Exception("the file does not exist", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("error parsing the file", ex);
            }
        }

        private void ParseTitles(string line)
        {
            string[] titleValues = line.Split(_delimitter);

            for (int i = 0; i < titleValues.Length; i++)
            {
                _titles[titleValues[i]] = i;
            }
            ValidateHeaders();
        }
        
        private void ValidateHeaders()
        {
            var properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(p => p.Name).ToHashSet();
            var missingHeaders = properties.Where(header => !_titles.Keys.Contains(header)).ToList();

            if (missingHeaders.Count > 0)
            {
                throw new Exception($"CSV headers do not match the properties of {typeof(T).Name}. Invalid headers: {string.Join(", ", missingHeaders)}");
            }
        }

        private List<T> CreateList()
        {
            return (List<T>)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeof(T)));
        }

        private T CreateInstance()
        {
            return (T)Activator.CreateInstance(typeof(T));
        }

        private void MapValuesToProperties(T instance, string line)
        {
            string[] values = line.Split(_delimitter);
            if (_titles.Count != values.Length)
            {
                throw new InvalidDataException($"row values mismatch headers \n\t{line}");
            }

            foreach (KeyValuePair<string, int> title in _titles)
            {
                PropertyInfo property = typeof(T).GetProperty(title.Key);
                if (property == null)
                    continue;

                string value = values[title.Value];

                // Remove surrounding quotes if present
                if (value.Length > 1 && value[0] == '"' && value[value.Length - 1] == '"')
                {
                    value = value.Substring(1, value.Length - 2);
                }

                object convertedValue;
        
                // Check if the property is an Enum
                if (property.PropertyType.IsEnum)
                {
                    try
                    {
                        convertedValue = Enum.Parse(property.PropertyType, value, true);
                    }
                    catch (ArgumentException)
                    {
                        throw new Exception($"invalid value '{value}' for enum {property.PropertyType.Name}");
                    }
                }
                else
                {
                    // Check if a conversion function has been provided
                    Func<string, object> conversionFunction = _conversionFunctions.ContainsKey(title.Key)
                        ? _conversionFunctions[title.Key]
                        : (x => Convert.ChangeType(x, property.PropertyType, _cultureInfo));

                    convertedValue = conversionFunction(value);
                }

                property.SetValue(instance, convertedValue);
            }
        }

    }
}

/*
    // Create a CSV parser for the "people.csv" file with a semicolon delimiter and a comma decimal separator.
    CsvParser<Person> parser = new CsvParser<Person>("people.csv", ';', ',');

    // Set the culture to "en-US" to use US-style date formatting.
    parser.SetCulture(new CultureInfo("en-US"));

    // Add a custom conversion function for the "Birthday" property to parse dates using the "dd/MM/yyyy" format.
    parser.AddConversionFunction("Birthday", x => DateTime.ParseExact(x, "dd/MM/yyyy", CultureInfo.InvariantCulture));

    // Parse the CSV file and get a list of Person instances.
    List<Person> people = parser.Parse();
 */